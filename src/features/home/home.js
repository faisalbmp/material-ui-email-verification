import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ButtonBase from '@material-ui/core/ButtonBase';
import { useHistory } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        margin: 'auto',
        maxWidth: 500,
        border: '1px solid black',
    },
    image: {
        width: 128,
        height: 128,
    },
    img: {
        margin: 'auto',
        display: 'block',
        maxWidth: '100%',
        maxHeight: '100%',
    },
    card:{
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2),
        border: '1px solid black',
    }
}));

export default function Home() {
    const classes = useStyles();
    const history = useHistory()

    return (
        <div className={classes.root}>
            <Paper className={classes.paper}>
            <Grid container justify="center" alignItems="center" spacing={10}>
                    <Grid item>
                        <ButtonBase className={classes.card} onClick={() => history.push("/login")}>
                            <Typography variant="body2" color="textSecondary">
                                Login
                </Typography>
                        </ButtonBase>
                    </Grid>
                    <Grid item>
                        <ButtonBase className={classes.card}  onClick={() => history.push("/register")}>
                            <Typography variant="body2" color="textSecondary">
                                Register
                </Typography>
                        </ButtonBase>
                    </Grid>
                </Grid>
            </Paper>
        </div>
    );
}