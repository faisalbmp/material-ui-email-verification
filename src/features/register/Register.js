import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ButtonBase from '@material-ui/core/ButtonBase';
import { useHistory } from "react-router-dom";
import { FormControl, FormHelperText, Input, InputLabel, Snackbar } from '@material-ui/core';
import { useState } from 'react';
import api from '../../config/api/Api';
import Alert from '../../component/Alert';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        margin: 'auto',
        maxWidth: 500,
        border: '1px solid black',
    },
    image: {
        width: 128,
        height: 128,
    },

    img: {
        margin: 'auto',
        display: 'block',
        maxWidth: '100%',
        maxHeight: '100%',
    },
    card: {
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2),
        border: '1px solid black',
    }
}));

export default function Register() {
    const classes = useStyles();
    const history = useHistory()
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [openSuccess, setOpenSuccess] = useState('')
    const [openError, setOpenError] = useState('')
    const [successMessage, setSuccessMessage] = useState('')
    const [errorMessage, setErrorMessage] = useState('')
    const [password, setPassword] = useState('')
    const submitRegister = (data) => {
        api.post('/register', {
            name,
            email,
            password
        }).then(res => {
            setSuccessMessage("Register Berhasil, cek email untuk lihat memverifikasi akun")
            setOpenSuccess(true)
        })
            .catch(err => {
                setErrorMessage(err.response.data.msg);
                setOpenError(true)
            })
    }
    const handleCloseSuccess = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenSuccess(false);
    }
    const handleCloseError = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenError(false);
    }
    return (
        <div className={classes.root}>
            <Paper className={classes.paper}>
                <Grid container direction="column" justify="center" alignItems="center" spacing={2}>
                    <Grid item>
                        <FormControl>
                            <InputLabel htmlFor="my-input">Name</InputLabel>
                            <Input value={name} onChange={(e) => setName(e.target.value)} id="my-input" aria-describedby="my-helper-text" />
                        </FormControl>
                    </Grid>
                    <Grid item>
                        <FormControl>
                            <InputLabel htmlFor="my-input">Email address</InputLabel>
                            <Input value={email} onChange={(e) => setEmail(e.target.value)} id="my-input" aria-describedby="my-helper-text" />
                        </FormControl>
                    </Grid>
                    <Grid item>
                        <FormControl>
                            <InputLabel htmlFor="my-input">Password</InputLabel>
                            <Input value={password} onChange={(e) => setPassword(e.target.value)} type="password" id="my-input" aria-describedby="my-helper-text" />
                        </FormControl>
                    </Grid>
                    <Grid item>
                        <ButtonBase className={classes.card} onClick={() => submitRegister()}>
                            <Typography variant="body2" color="textSecondary">
                                Register
                </Typography>
                        </ButtonBase>
                    </Grid>
                </Grid>
            </Paper>
            <Snackbar open={openSuccess} autoHideDuration={6000} onClose={handleCloseSuccess}>
                <Alert onClose={handleCloseSuccess} severity="success">
                    {successMessage}
                </Alert>
            </Snackbar>
            <Snackbar open={openError} autoHideDuration={6000} onClose={handleCloseError}>
                <Alert onClose={handleCloseError} severity="error">
                    {errorMessage}
                </Alert>
            </Snackbar>
        </div >
    );
}