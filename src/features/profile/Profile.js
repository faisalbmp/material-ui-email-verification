import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ButtonBase from '@material-ui/core/ButtonBase';
import { useHistory } from "react-router-dom";
import api from '../../config/api/Api';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        margin: 'auto',
        maxWidth: 500,
        border: '1px solid black',
    },
    image: {
        width: 128,
        height: 128,
    },
    img: {
        margin: 'auto',
        display: 'block',
        maxWidth: '100%',
        maxHeight: '100%',
    },
    card: {
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2),
        border: '1px solid black',
    }
}));

export default function Profile(props) {
    const classes = useStyles();
    const history = useHistory()
    const [user,setUser] = useState({})
    useEffect(()=>{
        api.post('/profile',{email:props.user.email})
        .then((res)=>{
            setUser(res.data)
        })
        .catch((err)=>{
            console.log('Err',err);
        })
    },[])
    return (
        <div className={classes.root}>
            <Paper className={classes.paper}>
                <Grid container justify="center" direction="column" alignItems="center" spacing={3}>
                    <Grid container item spacing={2}>
                        <Grid item>
                            <Typography variant="body2" color="textSecondary">
                                Name :
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Typography variant="body2" color="textSecondary">
                                {user.name}
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid container item spacing={2}>
                        <Grid item>
                            <Typography variant="body2" color="textSecondary">
                                Email :
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Typography variant="body2" color="textSecondary">
                                {user.email}
                            </Typography>
                        </Grid>
                    </Grid>
                <Grid item>
                    <ButtonBase className={classes.card} onClick={props.handleLogout}>
                        <Typography variant="body2" color="textSecondary">
                            Logout
                </Typography>
                    </ButtonBase>
                </Grid>
                </Grid>
            </Paper>
        </div>
    );
}