
import { useState } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  useHistory,
} from "react-router-dom";
import ProtectedRoute from "./component/ProtectedRoute";
import api from "./config/api/Api";
import ForgotPassword from "./features/forgotPassword/ForgotPassword";
import Home from "./features/home/home";
import Login from "./features/login/Login";
import Profile from "./features/profile/Profile";
import Register from "./features/register/Register";
import ResetPassword from "./features/resetPassword/ResetPassword";

function App() {
  const history = useHistory()
  const [user, setUser] = useState({})
  const [authenticated, setAuthenticated] = useState(false)

  const handleLogin = e => {
    setAuthenticated(true)
    setUser(e);
  
  }

  const handleLogout = e => {
    e.preventDefault();
    setUser({});
    setAuthenticated(false)
  }
  return (
    <Router>
      <div>
        {/* A <Switch> looks through its children <Route>s and
          renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/reset-password/:email/:token">
            <ResetPassword />
          </Route>
          <Route path="/forgot-password">
            <ForgotPassword />
          </Route>
          <Route path="/register">
            <Register />
          </Route>
          <Route path="/login" >
            <Login handleLogin={handleLogin} />
          </Route>
          <ProtectedRoute exact path='/profile' authenticated={authenticated} user={user} handleLogout={handleLogout} component={Profile} />
          <Route path="/">
            <Home />
          </Route>

        </Switch>
      </div>
    </Router>
  );
}

export default App;
