import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const ProtectedRoute = ({ component: Component,authenticated, ...rest }) => {
    console.log('zip',authenticated);
  return (
    <Route {...rest} render={
      props => {
        if (authenticated) {
          return <Component {...rest} {...props} />
        } else {
          return <Redirect to="/login" />
        }
      }
    } />
  )
}

export default ProtectedRoute;
